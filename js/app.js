$(document).ready(function() {
  goTo(0);
  setInterval(goToNext, 15000);

  $('.gif-item').click(function(e) {
    var index = $(e.currentTarget).data('index');
    goTo(index);
  });

  $('#gif-main').click(function(e) {
    var articleUrl = $(e.currentTarget).data('article-url');
    openArticle(articleUrl);
  });

  $(document).keypress(function(e) {
    if(!(e.keyCode === 13 || e.keyCode === 32)) return;

    var articleUrl = $('#gif-main').data('article-url');
    openArticle(articleUrl);
  });
});

function openArticle(url) {
  if(!url) return;

  window.open(url, '_top');
}

function goTo(index) {
  $('.gif-item').removeClass('active');

  var element = $('.gif-item[data-index=' + index + ']').get(0);

  var href = $(element).data('gif-src');
  var articleUrl = $(element).data('article-url');

  $('#gif-main').attr('src', href);
  $('#gif-main').data('article-url', articleUrl);

  $(element).addClass('active');
}

function goToNext() {
  var activeIndex = parseInt($('.active').data('index'));
  if(!(activeIndex >= 0)) return;

  var nextIndex = activeIndex >= 10 ? 0 : activeIndex + 1;
  goTo(nextIndex);
}
